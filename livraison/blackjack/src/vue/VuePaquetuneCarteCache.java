package vue;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.Observable;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.imageio.ImageIO;
import java.net.*;
import model.Carte;
import model.Paquet;

/**
 *
 * La classe qui nous permet d'affichr un paquet de cartes visibles
 */
public class VuePaquetuneCarteCache extends VuePaquet {
    
    private int wCarte,hCarte;
    private Image image;
    public Paquet paquet;
    public boolean UneCache;
/**
 * 
 * @param wCarte
 * @param hCarte
 * @param paquet 
 * Constructeur de pasquet de cartes visisbles
 * 
 */
    public VuePaquetuneCarteCache(int wCarte, int hCarte, Paquet paquet,boolean v) {
        super(wCarte, hCarte, paquet);
        this.wCarte = wCarte;
        this.hCarte= hCarte;
        this.paquet = paquet;
        this.UneCache=v;
        
    }

     /**
      * 
      * @param graphiques 
      * La fonction qui nous permet de donner le design l'interface 
      * 
      */
    @Override
    public void paintComponent(Graphics graphiques){
        
        super.paintComponent(graphiques);
        
        Color myColor=new Color(255, 255, 255);
        int j;
        // System.err.println("first paint, card nbr :"+nbrCartes);
       if(this.UneCache) {
          
       //dessinerCarte(x,y);
       //  la carte est cache
           if(this.paquet.getNbrCarte()>=1){
        	   j= paquet.getNbrCarte()-1;
        	   myColor=new Color(217, 217, 217);
               graphiques.setColor(myColor);
               //graphiques.fillRect((i*(wCarte+5))+wCarte, 10, wCarte, 80);
               graphiques.fillRoundRect((j*(wCarte+5))+wCarte, 10, wCarte, hCarte, 8, 8);
               //System.err.println(i);


               myColor=new Color(85, 85, 85);
               graphiques.setColor(myColor);

               graphiques.drawRoundRect((j*(wCarte+5))+wCarte, 10, wCarte, hCarte, 8, 8);

               myColor=new Color(255, 255, 255);
               graphiques.setColor(myColor);
       
             }
           else 
        	   j=0;        
       }
       else 
    	   j=paquet.getNbrCarte();
                    
                //la carte est visible
                    for (int i = 0 ; i < j ; i++){
                        
                        //dessinerCarte(x,y);
                        
                                
                            graphiques.setColor(myColor);
                            //graphiques.fillRect((i*(wCarte+5))+wCarte, 10, wCarte, 80);
                            graphiques.fillRoundRect((i*(wCarte+5))+wCarte, 10, wCarte, hCarte, 8, 8);
                            //System.out.println(i);
                            
                                
                            myColor=new Color(85, 85, 85);
                            graphiques.setColor(myColor);
                            
                             graphiques.drawRoundRect((i*(wCarte+5))+wCarte, 10, wCarte, hCarte, 8, 8);

                            // draw type of cards
                                graphiques.setFont(new Font("Times New Roman", Font.PLAIN, 12));
                                if(!paquet.getList().isEmpty())
                                {
                                    String s=paquet.getList().get(i).getCouleur();
                                    if(s.equals("coeur"))
                                    {
                                       try {
                                            image = ImageIO.read(new File(new File("images/heart.png").getAbsolutePath()));
                                            Image imageR = image.getScaledInstance(30,30, Image.SCALE_DEFAULT);
                                            graphiques.drawImage(imageR,(i*(wCarte+5))+wCarte+25,45,null);
                                            } catch (Exception e) {

                                            }
                                    }
                                    if(s.equals("pique"))
                                    {
                                        try {
                                            image = ImageIO.read(new File(new File("images/spade.png").getAbsolutePath()));
                                            Image imageR = image.getScaledInstance(30,30, Image.SCALE_DEFAULT);
                                            graphiques.drawImage(imageR,(i*(wCarte+5))+wCarte+25,45,null);
                                            } catch (Exception e) {

                                            }
                                    }
                                    if(s.equals("carreau"))
                                    {
                                        try {
                                            image = ImageIO.read(new File(new File("images/diamond.png").getAbsolutePath()));
                                            Image imageR = image.getScaledInstance(30,30, Image.SCALE_DEFAULT);
                                            graphiques.drawImage(imageR,(i*(wCarte+5))+wCarte+25,45,null);
                                            } catch (Exception e) {

                                            }
                                    }
                                    if(s.equals("trefle"))
                                    {
                                        try {
                                            image = ImageIO.read(new File(new File("images/club.png").getAbsolutePath()));
                                            Image imageR = image.getScaledInstance(30,30, Image.SCALE_DEFAULT);
                                            graphiques.drawImage(imageR,(i*(wCarte+5))+wCarte+25,45,null);
                                            } catch (Exception e) {

                                            }
                                    }

                                   // graphiques.drawString(s,(i*(wCarte+5))+wCarte+12,50);
                                }
                                
                            // draw value
                                graphiques.setFont(new Font("Times New Roman", Font.BOLD, 12));
                                String s2=paquet.getList().get(i).getHauteur();
                                switch(s2) {
                                case "dame":
                                  s2="Q";
                                  break;
                                case "roi":
                                  s2="K";
                                  break;
                                case "valet":
                                    s2="J";
                                    break;
                                case "as":
                                    s2="A";
                                    break;
                                default:
                                	break;
                                  // code block
                              }
                                graphiques.drawString(s2,(i*(wCarte+5))+wCarte+5,25);
                                myColor=new Color(255, 255, 255);
                                graphiques.setColor(myColor);
                            
                    }
                    
                   
                
                
                
        }
        
    

 
    public int getwCarte() {
        return wCarte;
    }
    
    public void setUneCachee(boolean v) {
    this.UneCache = v;}
/***
 * La fonction qui nous permet de mettre jour la vue 
   ***/
    @Override
    public void update(Observable o, Object arg) {
       dessiner();
    }

    
}