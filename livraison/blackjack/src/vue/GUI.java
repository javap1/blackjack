
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vue;
import java.util.*;
import controller.*;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.*;
import javax.swing.JButton;
import java.util.Optional;
import java.lang.Object;

import javax.swing.JOptionPane;
import javax.swing.*;
// import javax.swing.JDialog;
import java.awt.*;
import java.awt.event.*;

import vue.*;
import model.*;


/**
 * GUI ets la class de notre interface graphique 
 * 
 */
public class GUI extends JFrame{
    
    Paquet pMain,pPioche,pDefausse;
    public static Object valeur=0;
    VuePaquetuneCarteCache  vueMain,vueDefausse;
    VuePaquetCache vuePioche;
    JLabel labelmise;
    ControleurBtn controllerBtn,controllerBtn1,controllerBtn2,controllerBtn3,controllerBtn4;

    
    private final int CARTE_WIDTH = 80; 
    private final int CARTE_HEIGHT = 100;
    public GUI(){
    
        super("BlackJack");
        
        
        pPioche = Factory.jeu52();
        pMain =  Factory.getEmptyPaquet();
        pDefausse = Factory.getEmptyPaquet();
        pPioche.melanger();
        
        JLabel labelYou =new JLabel();
        
        vuePioche = new VuePaquetCache(CARTE_WIDTH, CARTE_HEIGHT, pPioche);
        vueMain = new VuePaquetuneCarteCache(CARTE_WIDTH, CARTE_HEIGHT, pMain,true); 
        vueDefausse = new VuePaquetuneCarteCache(CARTE_WIDTH,CARTE_HEIGHT, pDefausse,false);
        
        JButton btnStart=new JButton("Start");
        JButton btnTirer=new JButton("Tirer");
        JButton btnArreter=new JButton("Arreter");
        JButton btnReplay=new JButton("Replay");

        controllerBtn = new ControleurBtn(vuePioche,vueMain ,vueDefausse.paquet,btnStart,btnTirer,btnArreter,btnReplay,labelYou);
      
        JPanel mainPanel = vueMain;
        mainPanel.setBackground(new Color(0,102,0));

        JPanel panelCentre=new JPanel();
        panelCentre.add(btnStart);
        panelCentre.add(btnTirer);
        panelCentre.add(btnArreter);
        panelCentre.add(btnReplay);
        panelCentre.add(labelYou);
        panelCentre.setBackground(new Color(0,102,0));

        JPanel defaussePanel = vueDefausse;
        defaussePanel.setBackground(new Color(0,102,0));
        JPanel piochePanel = vuePioche;
        labelmise=new JLabel();
        piochePanel.add(labelmise);        
        piochePanel.setBackground(new Color(0,102,0));
        
        pPioche.addObserver(vuePioche);
        pMain.addObserver(vueMain);
        pDefausse.addObserver(vueDefausse);
        
        
       // this.setLayout(null);
        setSize(CARTE_WIDTH*(pMain.getNbrCarte()+5), 320);
        this.setLayout(new GridLayout(4, 1));
        this.add(piochePanel);
        this.add(mainPanel);
        this.add(panelCentre);
        this.add(defaussePanel);
        setAlwaysOnTop(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(700,500));
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
        
    }
    public void creerPanel(JFrame f)
    {
        JOptionPane jp=new JOptionPane();     
        Object[] possibleValues = { 1, 5,10,50,100,500 };
        int selectedValue = jp.showOptionDialog(f, "Choisir un mise", "mise",jp.YES_NO_OPTION,jp.QUESTION_MESSAGE, null,possibleValues, possibleValues[0]);
        if(selectedValue==-1)
        {
            System.exit(0);
        }
        else
        {
            valeur=possibleValues[selectedValue];
            labelmise.setText("la mise "+valeur);
        }
    }
    
}
