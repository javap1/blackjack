package model;

import model.*;


public class Player {
	private int mise;
	private int jeton;
	private Paquet paquet;


	public Player(Paquet paquet){
		this.jeton = 500;
		this.mise = 0;
		this.paquet=paquet;
	}

	
	public int getMise(){
		return this.mise;
	}

	public void initialiseMise(){
		this.mise = 0;
	}
	
	public void setMise(int n){
		this.mise+=n;
		this.jeton-=n;
	}


	public int getJeton(){
		return this.jeton;
	}

	
	public Paquet getPaquet(){
		System.out.println("Paquet"+this.paquet);
		return this.paquet;
	}


	public void setPaquet(Paquet paquet){
		this.paquet=paquet;
	}

	
	public void doublerMise(){
		if(this.jeton-this.mise >=0){
			this.jeton-=this.mise;
			this.mise=this.mise*2;
			
		}
	}

	
	public void gain(){
		int totalGain = this.mise*2;
		this.mise=0;
		this.jeton+=totalGain;
	}


	public void gainBlackjack(){
		int totalGain = this.mise*2+(this.mise/2);
		this.mise=0;
		this.jeton+=totalGain;
	}


	public void perte(){
		this.mise=0;
	}


	public void recupereMise() {
		this.jeton+=this.mise;
		this.mise=0;
	}



}