package model;

import model.*;

import java.util.*;


public class BlackJack{

	public Player joueur;
	public Paquet paq;
	public Paquet listCarte;
	public int valeur;
	public int nbAs=0;
	boolean fin = false;

	
	public BlackJack(Player joueur){
		this.listCarte=listCarte;
		this.joueur=joueur;
		this.listCarte=this.joueur.getPaquet();

	}

	public Paquet getPaquet(){
		return this.listCarte;
	}
	
	public boolean dix(Carte c){
		if(c.getHauteur() == "roi" || c.getHauteur() == "valet" || c.getHauteur() == "dame" || c.getHauteur() == "10"){
			return true;
		}
		return false;
	}

	public boolean getBlackjackWin(){
		if(this.listCarte.getNbrCarte()==2){
			if(dix(this.listCarte.getList().get(0)) && this.listCarte.getList().get(1).getHauteur()=="as"){
				return true;
			}
			if(dix(this.listCarte.getList().get(1)) && this.listCarte.getList().get(0).getHauteur()=="as"){
				return true;
			}
		}
		return false;
	}
	public void valeurTotalSansAs(){
		int compteur = 0;
		
		for(int i =0; i<this.getPaquet().getNbrCarte(); i++){
			Carte c = this.getPaquet().getList().get(i);
			if(c.getHauteur().equals("valet") || c.getHauteur().equals("dame") || c.getHauteur().equals("roi")){
				compteur+=10;
			}
			else 
			{
			if(c.getHauteur().equals("as")){		
				this.nbAs++;
			}
			else {
				int entier = Integer.parseInt(c.getHauteur());
				compteur+=entier;
			}
		    }
		}
		this.valeur = compteur;
	}
	public void choixAs(){
		int[] i = {1,2,3,4};
		if(this.nbAs!=0){
			System.out.println("this.nbAs="+this.nbAs);
			for(int n =0; n < 4 ; n++){
				if(this.nbAs == i[n]){
					if(this.valeur>(21-11-(i[n]-1))){
						this.valeur+=i[n];
					}
					else{
						this.valeur+=11+(i[n]-1);
					}
				}
			}
			System.out.println("this.valeur="+this.valeur);
		}
	}

	public String p()
	{
		return "p";
	}
	public int getTotal() {
		this.valeur=0;
		this.nbAs=0;
		this.valeurTotalSansAs();
		this.choixAs();
		return this.valeur;
	}
	
	public boolean plusDe21(){
		if(getTotal()>21) {
			return true;
		}
		return false;
	}
	
}