package controller;

import java.awt.Cursor;
import javax.swing.*;
import java.awt.event.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import model.Carte;
import model.Paquet;
import vue.*;
import model.*;


/**
 *
 * La classe qui nous permet d'avoir les cartes visibles du paquet 
 * 
 */
public class ControleurBtn implements MouseListener,ActionListener{

    private VuePaquetCache vueSource;
    private VuePaquetuneCarteCache vueMain;
    private Paquet dest1;
    private Paquet dest2;
    private JButton btnStart,btnTirer,btnArreter,btnRepaly,btn;
    private Player p,r;
    private BlackJack blackJackP,blackJackR;
    private JLabel label;
    private int valeurmise=0;
    public ControleurBtn(VuePaquetCache vueSource, VuePaquetuneCarteCache vueMain,Paquet dest2,JButton btnStart,JButton btnTirer,JButton btnArreter,JButton btnRepaly,JLabel label){
        this.vueSource =vueSource;
        this.vueMain=vueMain;
        this.dest1 = vueMain.paquet;
        this.dest2=dest2;
        this.btnStart=btnStart;
        this.btnTirer=btnTirer;
        this.btnArreter=btnArreter;
        this.btnRepaly=btnRepaly;
        this.label=label;
        p=new Player(dest2);
        r=new Player(vueMain.paquet);

        blackJackP=new BlackJack(p);
        blackJackR=new BlackJack(r);
        this.btnTirer.setEnabled(false);
        this.btnArreter.setEnabled(false);
        this.btnRepaly.setEnabled(false);
        this.btnStart.addActionListener(this);
        this.btnTirer.addActionListener(this);
        this.btnArreter.addActionListener(this);
        this.btnRepaly.addActionListener(this);
    }
 
     @Override
     public void mouseClicked(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
     }
 /**
  * 
  * @param e 
  * La fonction qui nous permet de retirer une carte dans le paquet quand on clique sur celle ci 
  */
     @Override
     public void mousePressed(MouseEvent e) {
     }
 
     @Override
     public void mouseReleased(MouseEvent e) {
         //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
     }
 
     @Override
     public void mouseEntered(MouseEvent e) {
         //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
         Cursor handCursor = new Cursor(Cursor.HAND_CURSOR);
         
         vueSource.setCursor(handCursor);
     }
 
     @Override
     public void mouseExited(MouseEvent e) {
         //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
         Cursor dCursor = new Cursor(Cursor.DEFAULT_CURSOR);
         vueSource.setCursor(dCursor);
     }
     public void tirer()
     {
        Carte carteCourante1 = this.vueSource.paquet.retirerCarte(0);
        this.dest2.ajouterCarte(carteCourante1);
        this.label.setText(" You: "+this.blackJackP.getTotal());
     }
     public void start()
     {
        Carte carteCourante1 = this.vueSource.paquet.retirerCarte(0);
        Carte carteCourante2 = this.vueSource.paquet.retirerCarte(0);
        Carte carteCourante3 = this.vueSource.paquet.retirerCarte(0);
        Carte carteCourante4 = this.vueSource.paquet.retirerCarte(0);
        this.dest1.ajouterCarte(carteCourante1);
        this.dest1.ajouterCarte(carteCourante2);
        this.dest2.ajouterCarte(carteCourante3);
        this.dest2.ajouterCarte(carteCourante4);
        this.label.setText(" You: "+this.blackJackP.getTotal());

     }

     public void arreter()
     {
        String decision="";
        int score=0;
        if(this.blackJackP.getBlackjackWin())
               decision+="BlackJack";
        else{
            if(this.blackJackP.getTotal()>21)
            {
                p.setMise((int )GUI.valeur);
                decision+="Vous avez perdu il vous reste "+p.getJeton();
                score=this.blackJackP.getTotal();
            }
            else {
                System.out.println("R"+this.blackJackR.getTotal());
            while(this.blackJackP.getTotal()>this.blackJackR.getTotal())
                {
                    Carte carteCourante1 = this.vueSource.paquet.retirerCarte(0);
                    this.dest1.ajouterCarte(carteCourante1);
                    for(Carte c :this.dest1.getList() )
                    {
                        System.out.println("la carte "+c);
                    }
                }

                if(this.blackJackR.getTotal()<=21)
                {
                    score=this.blackJackP.getTotal();
                    if(this.blackJackP.getTotal()==this.blackJackR.getTotal())
                    {
                        decision+="Egaaalitééé";
                    }
                    else 
                    {
                        p.setMise((int )GUI.valeur);
                        decision+="Vous avez perdu il vous reste "+p.getJeton();
                    }
                }
                else
                {
                    p.gain();
                    p.recupereMise();
                    decision+="Vous avez gagné votre score est "+p.getJeton();
                }
            } 
        }
        
        String newLine = System.getProperty("line.separator");
        this.label.setText(" "+decision+newLine+" Player: "+this.blackJackP.getTotal()+""+newLine+" Dealer: "+this.blackJackR.getTotal());
     }
     public void replay()
     {
          this.vueSource.paquet.addAll(dest1);
          this.vueSource.paquet.addAll(dest2);
          this.dest1.removeAll();
          this.dest2.removeAll();
          this.vueSource.paquet.melanger();
          this.label.setText(" Player: "+this.blackJackP.getTotal()+" Dealer: "+this.blackJackR.getTotal());


     }
     @Override
     public void actionPerformed(ActionEvent e) {
        //System.out.println("vvvvv"+this.btn.getText());
        if(e.getSource()==this.btnStart)
        {
            this.start();
            this.btnStart.setEnabled(false);
            this.btnTirer.setEnabled(true);
            this.btnArreter.setEnabled(true);
            this.btnRepaly.setEnabled(true);

        }
        if(e.getSource()==this.btnTirer)
        {
            this.tirer();
        }
        if(e.getSource()==this.btnArreter)
        {   this.vueMain.setUneCachee(false);
            this.arreter();
            this.vueMain.dessiner();
            this.btnTirer.setEnabled(false);
            this.btnArreter.setEnabled(false);
            //this.label.setText("Player: "+this.getTotalP()+" Dealer: "+this.getTotalR());
        }
        if(e.getSource()==this.btnRepaly)
        {   this.vueMain.setUneCachee(true);
            this.replay();
            this.btnStart.setEnabled(true);
            this.btnTirer.setEnabled(false);
        }
     }
    
}