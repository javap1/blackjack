package model;


public class Carte {
	public String hauteur;
	public String couleur;

	
	public Carte(String hauteur, String couleur){
		this.hauteur = hauteur;
		this.couleur = couleur;
	}

	
	public String getHauteur(){
		return this.hauteur;
	}

	public String getCouleur(){
		return this.couleur;
	}

	
	public String toString(){
		return this.hauteur +" de "+ this.couleur;
	}
}