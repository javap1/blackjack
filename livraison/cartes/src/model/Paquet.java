
package model;

import java.util.*;


public class Paquet extends Observable{
	public  ArrayList<Carte> list;
	public  String[] forme52={"as","2","3","4","5","6","7","8","9","10","valet","dame","roi"};
	public  String[] forme32={"as","7","8","9","10","valet","dame","roi"};
	public  String[] couleur={"pique","carreau","trefle","coeur"};

	public Paquet(){
		this.list = new ArrayList<Carte>();
	}
 
    // public static Paquet getPaquet52(){
         
    //      Paquet p = new Paquet() ;
    //      for(String color : couleur){
	// 		for(String form : forme52){
	// 			Carte c = new Carte(form,color);
	// 			p.list.add(c);
	// 		}
	// 	 }
    //      return p ;

    // }
    // public static Paquet getPaquet32(){
	// 	Paquet p = new Paquet() ;
	// 	for(String color : couleur){
	// 	   for(String form : forme32){
	// 		   Carte c = new Carte(form,color);
	// 		   p.list.add(c);
	// 	   }
	// 	}
	// 	return p ;


    // }

	public ArrayList<Carte> getList() {
		return this.list;
	}

	
	public int getNbrCarte() {
		return this.list.size();
	}

	  
	public String getHauteur(int i){
		Carte c = this.list.get(i);
		return c.getHauteur();
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////	
	public void addFirst(Carte c){
		this.list.add(0,c);
	}

	
	public void addLast(Carte c){
		this.list.add(c);
		
		
	}

	 public void ajouterCarte(Carte carte){
        this.list.add(carte);
        this.setChanged();
        this.notifyObservers();
        
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////
	public Carte tirage_aux_Hasard(){
		int position = (int)(Math.random()*this.list.size());
		return this.list.get(position);
	}

	
	public void melanger(){
		Collections.shuffle(this.list);
	}

	
	
////////////////////////////////////////////////////////////////////////////////////////////////////	   
	public void couper_Paquet(){
		int position = (int)(Math.random()*(this.list.size()-2)+1);
		List<Carte> liste1 = new ArrayList<Carte>(list.subList(0,position));
		this.list.removeAll(liste1);
		this.list.addAll(liste1);
		
		
		
		
		
		/*methode 2*
		 * int position = (int)(Math.random()*this.list.size());
		ArrayList<Cartes> liste1 = new ArrayList<Cartes>();
		ArrayList<Cartes> liste2 = new ArrayList<Cartes>();

		
		do{
			position = (int)(Math.random()*this.list.size());
		}while(position<6 || position>45);

		
		for(int i = 0 ; i<position ;i++){
			liste1.add(this.list.get(i));
		}
		
		for(int j = position; j< this.list.size(); j++){
			liste2.add(this.list.get(j));
		}
		this.list.clear();
		this.list.addAll(liste2);
		this.list.addAll(liste1);*/
	}
////////////////////////////////////////////////////////////////////////////////////////////////////////
     public void removeAll(Paquet p)
        {
           this.list.removeAll(p.getList());
		   this.setChanged();
       		this.notifyObservers();
        
        }
        
        
     public void removeAll()
     {
     	this.list.clear();
		 this.setChanged();
		 this.notifyObservers();
     }
     
     public void addAll(Paquet p)
     {
     this.list.addAll(p.getList());
	 this.setChanged();
       this.notifyObservers();
     }
     
     
     public void removeAll(ArrayList<Carte> l)
        {
           this.list.removeAll(l);
		   this.setChanged();
       	this.notifyObservers();
        
        }
     
     public void addAll(ArrayList<Carte> l)
     {
     	this.list.addAll(l);
		this.setChanged();
		this.notifyObservers();
     }
        
	
	
     public Carte retirerCarte(Carte c){
       if(!this.list.contains(c))
           return null;
	   this.list.remove(c);
       this.setChanged();
       this.notifyObservers();
	   return c;

     }
     
     
	 public Carte retirerCarte(int i){
	 	if(this.list.size()<=i) 
			return null;	
        Carte carte = this.list.get(i);
        this.list.remove(i);
        this.setChanged();
        this.notifyObservers();
        return carte;
        
    }
}
