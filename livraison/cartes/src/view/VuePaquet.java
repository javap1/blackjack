package view;

import java.util.Observer;
import javax.swing.JPanel;
import model.Paquet;

/**
 *
 * La classe de l'observateur 
 */
abstract class VuePaquet extends JPanel implements Observer{
    
        private int wCarte,hCarte,nbrCarte;
    
        public Paquet paquet;
        
        public VuePaquet(int wCarte, int hCarte,Paquet paquet){
            
            this.wCarte = wCarte;
            this.hCarte= hCarte;
            this.paquet = paquet;
            this.nbrCarte = paquet.getNbrCarte();
            
        }
         
       public void dessiner(){
           setNbrCarte();
           revalidate();
           repaint();
       }
       
       public void setNbrCarte(){
           this.nbrCarte = this.paquet.getNbrCarte();
       }
       
       public int getNbrCarte(){
           return this.nbrCarte;
       }
    
    
}