/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Observable;
import model.Paquet;

/**
 *
 * La classe des paquets de cartes cachées
 */
public class VuePaquetCache extends VuePaquet{
    
    
    
    private int wCarte,hCarte;
    public Paquet paquet;
/**
 * 
 * @param wCarte
 * @param hCarte
 * @param paquet 
 * Le constructeur de paquet de cartes cachées
 */
    public VuePaquetCache(int wCarte, int hCarte, Paquet paquet) {

        super(wCarte, hCarte, paquet);
        this.wCarte = wCarte;
        this.hCarte= hCarte;
        this.paquet = paquet;

    }
   /**
    * La fonction qi nous permet de dessiner ou donner le design de nos cartes
    * @param graphiques 
    */
    
    @Override
    public void paintComponent(Graphics graphiques){
        
        super.paintComponent(graphiques);
        
        Color myColor=new Color(217, 217, 217);
        
        for (int i = 0 ; i < paquet.getNbrCarte() ; i++){
                
                graphiques.setColor(myColor);
                //graphiques.fillRect((i*(wCarte+5))+wCarte, 10, wCarte, 80);
                graphiques.fillRoundRect((i*2), 10, wCarte, 80, 8, 8);
                //System.err.println(i);
                
                    
                myColor=new Color(85, 85, 85);
                graphiques.setColor(myColor);
                
                graphiques.drawRoundRect((i*2), 10, wCarte, 80, 8, 8);
                 
                myColor=new Color(217, 217, 217);
                graphiques.setColor(myColor);
                
        }
        
    }
    
    public int getwCarte() {
        return wCarte;
    }
    
    @Override
    public void update(Observable o, Object arg) {
        dessiner();
    }
    
 
    
}