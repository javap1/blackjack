
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controllor.*;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import view.*;
import model.*;

/**
 * GUI ets la class de notre interface graphique 
 * 
 */
public class GUI extends JFrame  {
    
    Paquet pMain,pPioche,pDefausse;
    
    VuePaquetVisible vueMain,vueDefausse;
    
    VuePaquetCache vuePioche;
    
    ControleurChoixCarteVersPaquet mainDefausseController,defaussePiocheController;
    ControleurPiocheVersPaquet cacheController;
    
    private final int CARTE_WIDTH = 70;
    
    private final int CARTE_HEIGHT = 80;
    
    
    public GUI(){
    
        super("Test vue");
        
        
        pPioche = Factory.jeu52();
        
        pMain = Factory.jeu52();

        //System.out.println("nbr cartes pioche : "+pPioche.getNbrCarte());

        pDefausse = Factory.getEmptyPaquet();
        
        pPioche.melanger();
        System.out.println(pPioche.getNbrCarte());
        //pMain.melanger();
        pDefausse.melanger();
        
        System.out.println("nbr cartes pioche : "+pPioche.getNbrCarte());
        
        System.out.println("nbr cartes main : "+pMain.getNbrCarte());
        
        System.out.println("nbr cartes defausse : "+pDefausse.getNbrCarte());
       
        
        vuePioche = new VuePaquetCache(CARTE_WIDTH, CARTE_HEIGHT, pPioche);
        
        vueMain = new VuePaquetVisible(CARTE_WIDTH, CARTE_HEIGHT, pMain);
        
        vueDefausse = new VuePaquetVisible(CARTE_WIDTH,CARTE_HEIGHT, pDefausse);
        
        
        
        mainDefausseController = new ControleurChoixCarteVersPaquet(vueMain, vueDefausse.paquet);
        defaussePiocheController = new ControleurChoixCarteVersPaquet(vueDefausse, vuePioche.paquet);
        cacheController = new ControleurPiocheVersPaquet(vuePioche, vueMain.paquet);
        
        
        JPanel mainPanel = vueMain;
        
        JPanel defaussePanel = vueDefausse;
        
        JPanel piochePanel = vuePioche;
        
        
        pPioche.addObserver(vuePioche);
        pMain.addObserver(vueMain);
        pDefausse.addObserver(vueDefausse);
        
        
       // this.setLayout(null);
        setSize(CARTE_WIDTH*(pMain.getNbrCarte()+5), 320);
        this.setLayout(new GridLayout(3, 1));
        this.add(piochePanel);
        this.add(mainPanel);
        this.add(defaussePanel);
        System.out.println("Aprés");
        System.out.println("nbr cartes pioche : "+pPioche.getNbrCarte());
        
        System.out.println("nbr cartes main : "+pMain.getNbrCarte());
        
        System.out.println("nbr cartes defausse : "+pDefausse.getNbrCarte());
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        
    }
    
}
