package view;

import controllor.*;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import view.*;
import model.*;

/**
 * GUI ets la class de notre interface graphique 
 * 
 */
public class GUIpioche extends JFrame  {
    
    Paquet pPioche;
    
    
    VuePaquetVisible vuePioche;
    
    
    private final int CARTE_WIDTH = 70;
    
    private final int CARTE_HEIGHT = 80;
    
    
    public GUIpioche(Paquet p){
        
        super("Pioche visible");
        
        
        pPioche = p;
        //System.out.println("nbr cartes pioche : "+pPioche.getNbrCarte());        
        pPioche.melanger();
        System.out.println(pPioche.getNbrCarte());
        //pMain.melanger();
        
        System.out.println("nbr cartes pioche : "+pPioche.getNbrCarte());
    
       
        
        vuePioche = new VuePaquetVisible(CARTE_WIDTH, CARTE_HEIGHT, pPioche);
        
        
        
       
        JPanel piochePanel = vuePioche;
        
        
        pPioche.addObserver(vuePioche);
       
        
        
       // this.setLayout(null);
        setSize(CARTE_WIDTH*(pPioche.getNbrCarte()+5), 320);
        this.setLayout(new GridLayout(3, 1));
        this.add(piochePanel);
        
        
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        
    }
    
}
