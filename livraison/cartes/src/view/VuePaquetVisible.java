
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Observable;
import model.Carte;
import model.Paquet;

/**
 *
 * La classe qui nous permet d'affichr un paquet de cartes visibles
 */
public class VuePaquetVisible extends VuePaquet{
    
    /**
	 * 
	 */


	private int wCarte,hCarte;
    
    public Paquet paquet;
/**
 * 
 * @param wCarte
 * @param hCarte
 * @param paquet 
 * Constructeur de pasquet de cartes visisbles
 * 
 */
    public VuePaquetVisible(int wCarte, int hCarte, Paquet paquet) {
        super(wCarte, hCarte, paquet);
        this.wCarte = wCarte;
        this.hCarte= hCarte;
        this.paquet = paquet;
        
    }

     /**
      * 
      * @param graphiques 
      * La fonction qui nous permet de donner le design à l'interface 
      * 
      */
    
    public void paintComponent(Graphics graphiques){
        
        super.paintComponent(graphiques);
        
        Color myColor=new Color(255, 255, 255);
        // System.err.println("first paint, card nbr :"+nbrCartes);
        for (int i = 0 ; i < paquet.getNbrCarte() ; i++){
            
            //dessinerCarte(x,y);
            // la carte est visible
                    
                graphiques.setColor(myColor);
                //graphiques.fillRect((i*(wCarte+5))+wCarte, 10, wCarte, 80);
                graphiques.fillRoundRect((i*(wCarte+5))+wCarte, 10, wCarte, hCarte, 8, 8);
                //System.out.println(i);
                
                    
                myColor=new Color(85, 85, 85);
                graphiques.setColor(myColor);
                
                 graphiques.drawRoundRect((i*(wCarte+5))+wCarte, 10, wCarte, hCarte, 8, 8);

                // draw type of cards
                    graphiques.setFont(new Font("Times New Roman", Font.PLAIN, 12));
                    if(!paquet.getList().isEmpty())
                    {
                        String s=paquet.getList().get(i).getCouleur();
                        graphiques.drawString(s,(i*(wCarte+5))+wCarte+12,50);
                    }
                    
                    
                // draw value
                
                    graphiques.setFont(new Font("Times New Roman", Font.BOLD, 12));
                    
                    String s2=paquet.getList().get(i).getHauteur();
                    graphiques.drawString(s2,(i*(wCarte+5))+wCarte+5,25);
                    myColor=new Color(255, 255, 255);
                    graphiques.setColor(myColor);
                    
                
                
                
                
        }
        
    }

 
    public int getwCarte() {
        return wCarte;
    }
/***
 * La fonction qui nous permet de mettre à jour la vue 
   ***/
    @Override
    public void update(Observable o, Object arg) {
       dessiner();
    }

    
}