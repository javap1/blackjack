
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllor;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import model.Carte;
import model.Paquet;
import view.VuePaquetCache;
import view.VuePaquetVisible;


/**
 *
 * La classe qui nous permet de de piocher des cartes non visibles
 */
public class ControleurPiocheVersPaquet implements MouseListener,ActionListener{
    
    private VuePaquetCache vueSource;
    
    private Paquet dest;
    
    private Carte carteCourante;
    
    private int cardIndex;
    
    public ControleurPiocheVersPaquet(VuePaquetCache vueSource, Paquet dest){
        
        this.vueSource =vueSource;
        
        this.dest = dest;
        
        vueSource.addMouseListener(this);
    
    }
    
    public void transferCard(){
        
       // vueSource.paquet.retirerCarte(cardIndex);
        dest.ajouterCarte(carteCourante);
        
        
        
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
        int x = e.getX()+5;
        
        int y = e.getY();
        
        int i = x/(vueSource.getwCarte()+5)-1;
        
        cardIndex = i;
        if(cardIndex<=-1)
            cardIndex ++;
        System.out.println("l index"+cardIndex);
        carteCourante = vueSource.paquet.retirerCarte(cardIndex);
        
        transferCard();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Cursor handCursor = new Cursor(Cursor.HAND_CURSOR);
        
        vueSource.setCursor(handCursor);
    }

    @Override
    public void mouseExited(MouseEvent e) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Cursor dCursor = new Cursor(Cursor.DEFAULT_CURSOR);
        vueSource.setCursor(dCursor);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
