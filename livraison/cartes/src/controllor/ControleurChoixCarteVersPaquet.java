
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllor;

import java.awt.Cursor;
import java.awt.event.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import model.Carte;
import model.Paquet;
import view.VuePaquetVisible;

/**
 *
 * La classe qui nous permet d'avoir les cartes visibles du paquet 
 * 
 */
public class ControleurChoixCarteVersPaquet implements MouseListener,ActionListener{
    
    private VuePaquetVisible vueSource;
    
    private Paquet dest;
    
    private Carte carteCourante;
    
    private int cardIndex;
    
    public ControleurChoixCarteVersPaquet(VuePaquetVisible vueSource, Paquet dest){
        
        this.vueSource =vueSource;
        
        this.dest = dest;
        
        vueSource.addMouseListener(this);
    
    }
    /**
     * La fonction qui nous permet de transferer la carte courante vers la destination
     */
    public void transferCard(){
        
       // vueSource.paquet.retirerCarte(cardIndex);
        dest.ajouterCarte(carteCourante);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
/**
 * 
 * @param e 
 * La fonction qui nous permet de retirer une carte dans le paquet quand on clique sur celle ci 
 */
    @Override
    public void mousePressed(MouseEvent e) {
       int x = e.getX()+5;
        
        int y = e.getY();
        
        int i = x/(vueSource.getwCarte()+5)-1;
        
        cardIndex = i;
        
        carteCourante = vueSource.paquet.retirerCarte(i);
        
        //carteCourante.setVisible(false);
        
        transferCard();
        
        //vueSource.dessiner();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Cursor handCursor = new Cursor(Cursor.HAND_CURSOR);
        
        vueSource.setCursor(handCursor);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        Cursor dCursor = new Cursor(Cursor.DEFAULT_CURSOR);
        vueSource.setCursor(dCursor);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}